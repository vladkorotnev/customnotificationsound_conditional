#import "CustomNotificationSound.h"
#import "CNSAppDetailController.h"

static NSDictionary *settingsDictionary;

static void LoadSettings(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo)
{
	[settingsDictionary release];
	settingsDictionary = nil;
	settingsDictionary = [[NSDictionary alloc] initWithContentsOfFile:@"/var/mobile/Library/Preferences/com.naken.customnotificationsound.plist"];	
}

%hook BBBulletin
- (BBSound *)sound
{
	NSString *bundleIdentifier = [self section];
	NSString *newSoundPath = settingsDictionary[bundleIdentifier];
	NSNumber *shouldSound = settingsDictionary[[bundleIdentifier stringByAppendingString:@"_"]];
	if([settingsDictionary objectForKey: [bundleIdentifier stringByAppendingString:@"_condition"]]) {
			NSString* conditionText = [settingsDictionary objectForKey: [bundleIdentifier stringByAppendingString:@"_condition"]];
			if(conditionText.length > 0) {
				if([self.content.message rangeOfString:conditionText options:NSCaseInsensitiveSearch].location != NSNotFound) {
					if ([newSoundPath isEqualToString:@"ORIGINAL"]) return %orig;
					else return [BBSound alertSoundWithSystemSoundPath:newSoundPath];
				}
			}
	}
	if (!self.content.message || !shouldSound || !newSoundPath) return %orig;
	if (![shouldSound boolValue]) return %orig;
	else if ([newSoundPath isEqualToString:@"ORIGINAL"]) return %orig;
	return [BBSound alertSoundWithSystemSoundPath:newSoundPath];
}
%end

%group NotificationSettingsHook

%hook BulletinBoardAppDetailController

%new
- (NSNumber *)soundsValue
{
	if (kCFCoreFoundationVersionNumber < kCFCoreFoundationVersionNumber_iOS_9_0) return [self _valueOfNotificationType:0x10];
	return [self _valueOfNotificationType:0x10 forSectionInfo:[self.specifier propertyForKey:@"BBSECTION_INFO_KEY"]];
}

%new
- (BOOL)isSoundsSectionInCell:(PSTableCell *)visibleCell
{
	if (kCFCoreFoundationVersionNumber < kCFCoreFoundationVersionNumber_iOS_9_0) return [visibleCell.specifier.identifier isEqualToString:@"SOUNDS"];
	return [visibleCell.specifier.identifier isEqualToString:@"SOUNDS_ID"];
}

- (float)tableView:(UITableView *)arg1 heightForRowAtIndexPath:(NSIndexPath *)arg2
{
	if (arg2.section == 1)
	{
		for (PSTableCell *visibleCell in [arg1 visibleCells])
		{
			if ([self isSoundsSectionInCell:visibleCell])
			{
				visibleCell.accessoryView = nil;
				visibleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				visibleCell.textLabel.text = NSLocalizedStringFromTableInBundle(@"CUSTOMSOUND", @"Localizable", [NSBundle bundleWithPath:@"/Library/Application Support/CustomNotificationSound/CustomNotificationSound.bundle"], nil);
				NSString *detailText = [settingsDictionary[self.specifier.identifier] lastPathComponent];
				if (!detailText) detailText = @"ORIGINAL";
				if (![[self soundsValue] boolValue]) detailText = @"NONE";
				visibleCell.detailTextLabel.text = NSLocalizedStringFromTableInBundle(detailText, @"Localizable", [NSBundle bundleWithPath:@"/Library/Application Support/CustomNotificationSound/CustomNotificationSound.bundle"], nil);
				break;
			}
		}
	}
	return %orig;
}

- (void)tableView:(UITableView *)arg1 didSelectRowAtIndexPath:(NSIndexPath *)arg2
{
	if (arg2.section == 1 && arg2.row == 1 && [[arg1 cellForRowAtIndexPath:arg2] isKindOfClass:NSClassFromString(@"PSSwitchTableCell")])
	{
		CNSAppDetailController *controller = [[CNSAppDetailController alloc] init];
		controller.valueOfNotificationType = [self soundsValue];
		controller.pusherController = self;
		controller.bundleIdentifier = self.specifier.identifier;
		controller.chosenSoundPath = settingsDictionary[controller.bundleIdentifier];
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
	}
	else %orig;
}
%end

%end

%hook PSListController
- (void)lazyLoadBundle:(PSSpecifier *)arg1
{
	%orig;
	if ([[arg1 identifier] isEqualToString:@"NOTIFICATIONS_ID"]) %init(NotificationSettingsHook);
}
%end

%ctor
{
	if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_8_0)
	{
		%init;
		if ([[[NSProcessInfo processInfo] processName] isEqualToString:@"SpringBoard"])
		{
			NSString *settingsPath = @"/var/mobile/Library/Preferences/com.naken.customnotificationsound.plist";
			if (![[NSFileManager defaultManager] fileExistsAtPath:settingsPath]) [[NSDictionary dictionary] writeToFile:settingsPath atomically:YES];
		}
		CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, LoadSettings, CFSTR("com.naken.customnotificationsound.settingschanged"), NULL, CFNotificationSuspensionBehaviorCoalesce);
		LoadSettings(NULL, NULL, NULL, NULL, NULL);
	}
}
